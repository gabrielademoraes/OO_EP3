json.extract! tarefa, :id, :title, :content, :created_at, :updated_at
json.url tarefa_url(tarefa, format: :json)
