Rails.application.routes.draw do
  resources :tarefas
  devise_for :users, controllers: { registrations: "registrations"}
  root 'home#index'
  get 'login' => 'home#login'
  get 'tarefa', to:'home#tarefa'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
